% La boucle de calcul 
ii = 1;
cci =0;    ccj =0;   cck =0;
while TempsT < Time;
    % calcul de la temperature au condition au limite 1 (noeud i = 1)
    Tp(1,1)      = CST1*T(1,1)+ CST2*T(1,2);
    % calcul de la Temperature a l'interieur de la matrice (1 < i < n)
    for jj = 2:1:n-1;
        Tp(1,jj) = CST3*T(1,jj-1) + CST4*T(1,jj) +  CST5*T(1,jj+1);
    end;
    % calcul de la Temperature au condition au limite 2 (noeud i = n)
    Tp(1,n) = CST6*T(1,n-1) + CST7*T(1,n) + CST8*Tinfini;
   
    % Affichage graphique des resultats 
    %%%%%%%%%%%%% Voir Annexe %%%%%%%%%%%%%%
    % Changement de couleur
    %%%%%%%%%%%%% Voir Annexe %%%%%%%%%%%%%%
    
    % Boucle de changement de couleur: pas necessaire 

    % Mise a jour du temps et de la temperature  
    TempsT = TempsT + dt;
    T = Tp;
    ResExp(ii+1,1:Col+1)=[TempsT T];
    ii = ii + 1;
end;