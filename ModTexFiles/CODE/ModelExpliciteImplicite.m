%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Programme permettant de visualiser la distribution
% de chaleur dans une saucisse
%
% Resolution numerique de l'equation de Fick :
%   dT          ( 1    dT    d2T  )
%   -- = alpha .( - . --- + ----  )
%   dt          ( r    dR    dR2  )
% Conditions aux limites :
% Bout 1 : Centre et Symetrie :
% 0 = k A dT/dr
%
% Bout 2 : Convection :
% h A (T inifini - T surface)= k A dT/dr
% Methode numerique de resolution: elements finis
% Comparaison entre la methode numerique explicite 
% et la methode implicite
% Dans le cadre du cours: Operations Unitaires II
% Date: Hivers 2019
% Professeur: Seddik Khalloufi
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
% Initialisation de Matlab
clear all
close all
clc
 
% Parametres/Donnees en SI
Ro       = 970;         % Kg/m3     970-1070
Cp       = 2850;        % J/kg K    2850-3380
K        = 0.48;        % W/mK      0.26-0.48
alpha    = K/(Ro*Cp);   % m2/s
Rmax     = 0.1;         % m
Tinfini  = 80;          % oC
Tini     = 0;           % oC
h        = 792;         % W/m2K     792-2107
Time     = 2*3600;      % s

% Choix du nombre de noeuds
n      = 4*10; % peut etre augmente une fois le programe marche

% Calcul du pas en m  :distance
dr     = Rmax/(n-1);
% Condition de convergence dt
dt1    = (dr^2)/(2*alpha);
dt2    = (dr^2)/(4*alpha); %dt1>dt2
dt3    = (Ro*Cp*dr^2*(4*Rmax-dr))/((8*Rmax)*(h*dr+alpha));
vdt    = 0.99*[dt1,dt2,dt3];

dt     = min(vdt);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% Constantes %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Constantes generiques
CST9  = (alpha*dt)/(dr)^2;        %CONST GEN1
CST10 = (alpha*dt)/(2*Rmax*dr);   %CONST GEN2

% Calcul de certaines constantes i=1 et r=0
CST1  = 1 - 4*CST9;   %Tp @ i=1   ici explicite -> implicite => 1 + 4*CST9;
CST2  = 4*CST9;       %Tp @ i=2

% Calcul de certaines constantes 1<i<N et 0<r<R
CST3  = CST9 - CST10;   %Tp @ i-1
CST4  = 1 - 2*CST9;     %Tp @ i  ici explicite -> implicite => 1 + 2*CST9;
CST5  = CST9 + CST10;   %Tp @ i+1

% Calcul de certaines constantes refroidissement
CST8  = (8*Rmax*h*dt)/(Ro*Cp*dr*(4*Rmax-dr));       %Tp @ i=infinity
CST6  = (8*Rmax*alpha*dt)/(Ro*Cp*dr^2*(4*Rmax-dr)); %TP @ i=n-1
CST7  = 1-CST8-CST6;                                %TP @ i=n
% ici explicite -> implicite => 1 +CST8+CST6;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% Explicite %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% formation du vecteur distance
Rexp   = 0:dr:Rmax;
% Initialisation 
TempsT = 0;
T      = Tini*ones(1,n);

% Affichage du graphique explicite
figure
plot(Rexp,T,'-r.');
xlabel('Distance en m');
ylabel('Temperature en oC');
title('Profils des temperatures avec la resolution explicite');
hold on

% Determination de la matrice qui va contenir les resultats 
Ligne   = ceil(Time/dt);
Col     = n;
ResExp = zeros(Ligne+1,Col+1);
ResExp(1,1:Col+1)= [TempsT T];


% La boucle de calcul 
ii = 1;
cci =0;    ccj =0;   cck =0;
while TempsT < Time;
    % calcul de la temperature au condition au limite 1 (noeud i = 1)
    Tp(1,1)      = CST1*T(1,1)+ CST2*T(1,2);
    % calcul de la Temperature a l'interieur de la matrice (1 < i < n)
    for jj = 2:1:n-1;
        Tp(1,jj) = CST3*T(1,jj-1) + CST4*T(1,jj) +  CST5*T(1,jj+1);
    end;
    % calcul de la Temperature au condition au limite 2 (noeud i = n)
    Tp(1,n)      = CST6*T(1,n-1) + CST7*T(1,n) + CST8*Tinfini;
   
    % Affichage graphique des resultats 
    plot(Rexp,Tp,'color',[cci ccj cck]);
    hold on;
    
    % Boucle de changement de couleur: pas necessaire 
      if cci < 0.9 
          cci = cci + 0.01; 
      else
          if ccj  < 0.9
              ccj = ccj + 0.01;
          else
             cck = cck + 0.01;
             cci =0;
             ccj =0;
          end
      end;
         
    % Mise a jour du temps et de la temperature  
    TempsT = TempsT + dt;
    T = Tp;
    ResExp(ii+1,1:Col+1)=[TempsT T];
    ii = ii + 1;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% Implicite %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Changement des constantes explicite pour implicite 
CST1     = 1 + 4*CST9;          %Tp @ i=1 avant =>1 - 4*CST9;
CST4     = 1 + 2*CST9;          %Tp @ i   avant =>1 - 2*CST9;
CST7     = 1 + CST8 + CST6;     %Tp @ i=n avant =>1 - CST8 - CST6;

Rimp      = 0:dr:Rmax; % formation du veteur Distance

T      = Tini*ones(1,n); % Formation du veteur Temperature

% Affichage du graphique implicite
figure
plot(Rimp,T,'-r.');
xlabel('Distance en m');
ylabel('Temperature en oC');
title('Profils des temperatures avec la resolution implicite','Color', 'm');
hold on

% Initialisation du temps
TempsT = 0;

% Formation de la matrice: ResImp
        Ligne   = ceil(Time/dt); % Nombre de lignes
        Col     = n;             % Nombre de colonnes
ResImp = zeros(Ligne+1,Col+1);
ResImp(1,1:Col+1) = [TempsT T];

% indices des coleurs pour affichage en couleur : pas necessaire
cci =0;    ccj =0;   cck =0.5;

% formation de la matrice constante
Matrice = zeros(n,n); 
Matrice(1,1)   =  CST1;
Matrice(1,2)   =  -CST2;
for LL = 2:1:n-1;
      for CC = 1:1:n;
          if LL == CC +1;
              Matrice(LL,CC) =  -CST3;
          end
          if LL == CC;
           Matrice(LL,CC) =   CST4;
          end
          if CC == LL +1
              Matrice(LL,CC) =  -CST5;
          end
       end;
end
Matrice(n,n-1) = -CST6;
Matrice(n,n)   = CST7;
T    = T';

% Calcul de la matrice inverse 
invM = inv(Matrice);

% La boucle de calcul 
ii = 1;

while TempsT < Time;
    T(1,1)         = T(1,1);
    T(n,1)         = CST8*Tinfini + T(n,1);
    Tp = invM*T;
    % Affichage graphique des resultats
    plot(Rimp,Tp,'color',[cci ccj cck]);
    hold on;
    
    % Boucle de changement de couleur: pas necessaire
    if cci < 0.9
        cci = cci + 0.01;
    else
        if ccj  < 0.9
            ccj = ccj + 0.01;
        else
            cck = cck + 0.01;
            cci =0;
            ccj =0;
        end
    end;
    
    hold on;
    
    
    % Mise a jour du temps et de la temperature
    TempsT = TempsT + dt;
    T = Tp;
    ResImp(ii+1,1:Col+1)=[TempsT T'];
    ii = ii + 1;
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% Comparaison %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Variable adimensionnelle de Fourier
Fo      = (alpha/Rmax^2)*ResImp(1:ii,1);

% Retour aux temps en s
Tempstime= (Rmax^2/alpha)*Fo;
% Graphique des courbes de temperatures
figure
xlabel('Temps (s)');
ylabel('Temperature (oC)');
title('Comparaison des deux methodes: Explicite & Implicite ');
hold on ;
plot(Tempstime,ResExp(1:ii,n),'k.',Tempstime,ResImp(1:ii,n),'-m');
plot(Tempstime,ResExp(1:ii,n-2),'k.',Tempstime,ResImp(1:ii,n-2),'-m');
plot(Tempstime,ResExp(1:ii,n/2+15),'k.',Tempstime,ResImp(1:ii,n/2+15),'-m');
plot(Tempstime,ResExp(1:ii,n/2+10),'k.',Tempstime,ResImp(1:ii,n/2+10),'-m');
plot(Tempstime,ResExp(1:ii,n/2+5),'k.',Tempstime,ResImp(1:ii,n/2+5),'-m');
plot(Tempstime,ResExp(1:ii,n/2),'k.',Tempstime,ResImp(1:ii,n/2),'-m');
plot(Tempstime,ResExp(1:ii,n/4),'k.',Tempstime,ResImp(1:ii,n/4),'-m');
legend('Solution Explicite','Solution Implicite','Location','Best');