% formation de la matrice constante
Matrice = zeros(n,n); 
Matrice(1,1)   =  CST1;
Matrice(1,2)   =  -CST2;
for LL = 2:1:n-1;
      for CC = 1:1:n;
          if LL == CC +1;
              Matrice(LL,CC) =  -CST3;
          end
          if LL == CC;
           Matrice(LL,CC) =   CST4;
          end
          if CC == LL +1
              Matrice(LL,CC) =  -CST5;
          end
       end;
end
Matrice(n,n-1) = -CST6;
Matrice(n,n)   = CST7;
T    = T';

% Calcul de la matrice inverse 
invM = inv(Matrice);

% La boucle de calcul 
ii = 1;

while TempsT < Time;
    T(1,1)         = T(1,1);
    T(n,1)         = CST8*Tinfini + T(n,1);
    Tp = invM*T;

    % Affiche le graphique et changement de couleur
    %%%%%%%%%%%%% Voir Annexe %%%%%%%%%%%%%%
    
    % Mise a jour du temps et de la temperature
    TempsT = TempsT + dt;
    T = Tp;
    ResImp(ii+1,1:Col+1)=[TempsT T'];
    ii = ii + 1;
end;