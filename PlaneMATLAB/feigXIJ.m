% feigXIJ(m,B1,B2).m 6/20/07 Matlab subroutine written by James V. 
% Beck based on programming and method of Prof. A. Haji-Sheikhi,
% Haji-Sheikh, A. and Beck, J.V., 
% "An Efficient Method of Computing Eigenvalues in Heat Conduction",
% Numerical Heat Transfer Part B: Fundamentals, Vol 38 No. 2, pp. 133-156, 2000
% It gives the eigenvalues for the X11, X12, X13, ..., X33 cases. 
% X33 is the general case & includes the others.
% However, for X11 and X22, the eigenvalues are n *pi, for n = 1, 2, 3, ... .
% (Also n = 0 for X22.) For X12 and X21, the eigenvalues are (2*n-1)*pi/2
% for n = 1, 2, 3, 
function XIJ=feigXIJ(m,B1,B2)
    % Input quantities
    % B1 is the one of the Biot numbers. For X31, it is for Bi_1.
    % B2 is the other Biot number. For X13, it is for Bi_2. 
    % The eigenvalues for X13 and X31 are the same.
    % For the boundary condition of the first kind (given temperature), 
    % Bi value goes to infinity.
    % Use BI1=1.0e+15 or BI2 = 1.0e+15.
    % For the case of bc of the second kind, the Bi value goes to zero. 
    % Use BI1=0 or BI2 = 0
    % m is the number of eigenvalues
    % Accuracy: Machine accuracy is expected, about 14 decimal place accuracy
    BI1=B1; BI2=B2; AN=1:1:m;
    % Immediately below generates the X33 eigenvalues using the Haji subroutine
    CN =(AN-0.75)*pi; DN=(AN-0.5)*pi; D=1.22*(AN-1)+0.76; P23 = 1./AN;
    BX = BI1*BI2; BS = BI1 + BI2;
    GAM = 1.0- 1.04*(sqrt((BS + BX+CN-pi/4.0)./(BS +BX+D))...
        -(BS+BX+sqrt(D.*(CN-pi/4.0)))./(BS+BX+D));
    X23 = (BI1+BI2-CN)./(BI1+BI2+CN); X13 ...
        = BX./(BX+0.2+BS*(pi*pi*(AN-0.5)/2.0));
    G13=1+X13.*(1-X13).*...
        (1-(0.85./AN)-(0.6-0.71./AN).*(X13+1).*(X13-0.6-0.25./AN));
    E23 = GAM.*(P23.*X23 + (1.0 - P23).*tanh(X23)/tanh(1)) ;
    E13 = 2.0*G13.*X13; Y23=CN+pi*E23/4.0; Y13=DN+pi*E13/4.0;
    ZN =(Y13*BX./(BX+DN.^2)+Y23.*(1.0-BX./(BX+DN.^2)));
    for i=1:3
        A0=((6.0+3.0*BS+BX-ZN.*ZN).*cos(ZN)-(6.0+BS)*ZN.*sin(ZN))/6.0;
        B0=(ZN.^2-BS-BX).*cos(ZN)+(2.0+BS)*ZN.*sin(ZN);
        C0=(ZN.^2-BX).*sin(ZN)-BS*ZN.*cos(ZN); 
        A0=(1.0+BS)*sin(ZN)+2.0*ZN.*cos(ZN)-C0/2.0;
        E0=-C0./B0-...
            (-A0.*C0.^3+A0.*C0.^2 .*B0)./...
            (3.0*A0.*C0.*C0.*B0 -2.0*A0.*C0.*B0.^2+B0.^4);
        ZN = ZN+E0;
    end
    if BI1 == 0 & BI2 == 0
        ZN(1)=0;
    end
    num=1:1:m;
    for ie=1:m
        XIJintrinsic(ie)=tan(ZN(ie))-ZN(ie)*(B1+B2)/(ZN(ie)^2-B1*B2);
    end%ie
    XIJ=[num' ZN' XIJintrinsic'];
    %fprintf('%7.1f %15.15f %15.5e \n',XIJ')
