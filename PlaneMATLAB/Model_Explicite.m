%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Programme permettant de: xxxxxxxxxx
% Resolution numerique de l'equation de Fick :
%      dT                d2T
%      --   =     alpha ----
%      dt                dX2
% Conditions aux limites :
% Bout 1 : convection :
% h A (T inifini - T surface)= k A dT/dX
% Bout 2 : Isole: 
% k A dT/dX =0
% Methode numerique de resolution: elements finis
%                         %%%%%%%%%%%%%%%%%%%%%%%%%
% Schema de la resolution:%%%%%%%   EXPLICITE  %%%% 
%                         %%%%%%%%%%%%%%%%%%%%%%%%%
% Comparaison entre la methode analytique et methode numerique
% Dans le cadre du cours: Operations Unitaires II
% Date: Hivers 2019
% Professeur: Seddik Khalloufi
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialisation de Matlab
clear all
close all
clc

% Parametres/Donnees en SI
Ro       = 1000;        % Kg/m3 
Cp       = 4200;        % J/kg K 
K        = 0.6;         % W/mK
alpha    = K/(Ro*Cp);   % m2/s 
L        = 0.20;        % m 
Tinfini  = 100;         % oC
Tini     = 0;           % oC 
h        = 500;         % W/m2K
Time     = 4*3600;      % s 

% Choix du nombre de noeuds : peut etre adapte 
n      = 8*10;
% Calcul du pas dans l'espace
dx     = L/(n-1);

% Condition de convergence dt
dt     = .99*min(dx^2/(2*alpha),((1/2)*(Ro*Cp*dx^2))/(h*dx + alpha*Ro*Cp));

% Condition pour provoquer l'instabilite du systeme (divergence 
% de la solution) . vous pouvez l'essayer pour voir le phenomene mathematique  
% dt     = 10*dt;  

% Calcul de certaines constantes  
CST1    = (alpha*dt)/dx^2;
CST2    = (2*h*dt)/(Ro*Cp*dx);
CST3    = 1 - 2*CST1 - CST2 ;
CST4    = 1 - 2*CST1;
% formation du vecteur distance
X      = 0:dx:L;
% Initialisation 
TempsT = 0;
T      = Tini*ones(1,n);
% Affichage des conditions initiales : pas necessaire mais c'est pour verification
plot(X,T,'-r.');
xlabel('Distance en m');
ylabel('Temperature en oC');
title('Profils des temperatures');
hold on

% Determination de la matrice qui va contenir les resultats 
Ligne   = ceil(Time/dt);
Col     = n;
Res = zeros(Ligne+1,Col+1);
Res(1,1:Col+1)= [TempsT T];

% La boucle de calcul 
ii = 1;
cci =0;    ccj =0;   cck =0;
while TempsT < Time;
    % calcul de la temperature au condition au limite 1 (noeud i = 1)
    Tp(1,1)      = CST3*T(1,1) + 2*CST1*T(1,2) + CST2*Tinfini;
    % calcul de la Temperature a l'interieur de la matrice (1 < i < n)
    for jj = 2:1:n-1;
        Tp(1,jj) = CST4*T(1,jj) + CST1*(T(1,jj-1) + T(1,jj+1));
    end;
    % calcul de la Temperature au condition au limite 2 (noeud i = n)
    Tp(1,n)      = CST4*T(1,n) + 2*CST1*T(1,n-1);

    % Affichage graphique des resultats 
    plot(X,Tp,'color',[cci ccj cck]);
    hold on;

    % Boucle de changement de couleur: pas necessaire 
    if cci < 0.9 
        cci = cci + 0.01; 
    else
        if ccj  < 0.9
            ccj = ccj + 0.01;
        else
            cck = cck + 0.01;
            cci =0;
            ccj =0;
        end
    end;

    % Mise a jour du temps et de la temperature  
    TempsT = TempsT + dt;
    T = Tp;
    Res(ii+1,1:Col+1)=[TempsT T];
    ii = ii + 1;
end;
% % % Affichage des resultats sous forme de tableau: pas necessaire
% % printmat(Res,'Resultats')

% Parametres addimentionnels necessaire pour 
% la comparaison avec la methode analytique ;
Biot     = h*L/K;
Xbar    =  X'/L;
Fo      = (alpha/L^2)*Res(1:ii,1);
% Utilisation de la solution analytique;
Precision = 10;
[Texact] = fdX32B10T0(Xbar,Fo,Biot,Precision);

% Conversion 
% Retour aux tempertaures en oC
for jj=1:1:n;
    Texact(1:ii,jj)  = Tinfini*Texact(1:ii,jj);
end
% Retour aux temps en s
Tempstime= (L^2/alpha)*Fo;

% Comparaison des deux methodes: Analytique et numerique
figure
xlabel('Temps (s)');
ylabel('Temperature (oC)');
title('Comparaison des deux methodes: Analytique & Numerique ');
hold on ;
plot(Tempstime,Texact(1:ii,1),'r.',Tempstime,Res(1:ii,2),'-b');
plot(Tempstime,Texact(1:ii,2),'r.',Tempstime,Res(1:ii,3),'-b');
plot(Tempstime,Texact(1:ii,n/8),'r.',Tempstime,Res(1:ii,n/8+1),'-b');
plot(Tempstime,Texact(1:ii,n/4),'r.',Tempstime,Res(1:ii,n/4+1),'-b');
plot(Tempstime,Texact(1:ii,n/2),'r.',Tempstime,Res(1:ii,n/2+1),'-b');
plot(Tempstime,Texact(1:ii,n),'r.',Tempstime,Res(1:ii,n+1),'-b');
