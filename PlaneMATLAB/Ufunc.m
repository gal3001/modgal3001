% utility function to return U(x,t,B1)
function U=Ufunc(x,t,B1)
% U=exp(B1*x+B1^2*t)*erfc(x/sqrt(4*t)+B1*sqrt(t));
% expression below better behaved esp. for large Bi
U = exp(-x^2/4/t)*erfcx( x/sqrt(4*t)+B1*sqrt(t) );
end
