%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Programme permettant de: xxxxxxxxxx
% Resolution numerique de l'equation de Fick :
%      dT                d2T
%      --   =     alpha ----
%      dt                dX2
% Conditions aux limites :
% Bout 1 : convection :
% h A (T inifini - T surface)= k A dT/dX
% Bout 2 : Isole:
% k A dT/dX =0
% Methode numerique de resolution: elements finis
%                         %%%%%%%%%%%%%%%%%%%%%%%%%
% Schema de la resolution:%%%%%%%  IMPLICITE  %%%%%
%                         %%%%%%%%%%%%%%%%%%%%%%%%%
% Comparaison entre la methode analytique et methode numerique
% Dans le cadre du cours: Operations Unitaires II
% Date: Hivers 2019
% Professeur: Seddik Khalloufi
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialisation de Matlab
clear all
close all
clc

% Parametres/Donnees en SI
Ro       = 1000;        % Kg/m3
Cp       = 4200;        % J/kg K
K        = 0.1;         % W/mK
alpha    = K/(Ro*Cp);   % m2/s
L        = 0.1;        % m
Tinfini  = 100;         % oC
Tini     = 0;           % oC
h        = 50;         % W/m2K
Time     = 10*3600;      % s

% Choix du nombre de noeuds
n      = 8*10; % peut etre augmente une fois le programe marche

% Calcul du pas en m: distance
dx     = L/(n-1);
% Calcul du pas en s: temps
dt     = 100;

% Calcul de certaines constantes
CST1     = alpha*dt/dx^2;
CST2     = 1 + 2*CST1;
CST3     = 2*dt*K + 2*dt*h*dx + Ro*Cp*dx^2;
CST4     = 2*K*dt;
CST5     = 2*dt*h*dx*Tinfini;
CST6     = Ro*Cp*dx^2;
CST7     = CST4 + CST6 ;

% formation du veteur Distance
X      = 0:dx:L;
% Formation du veteur Temperature
T      = Tini*ones(1,n);

% affichage dans un graphique : pas necessaire mais c'est pour verifier
plot(X,T,'-r.');
xlabel('Distance en m');
ylabel('Temperature en oC');
title('Profils des temperatures');
hold on

% Initialisation du temps
TempsT = 0;

% Formation de la matrice: Res
% Nombre de lignes
Ligne   = ceil(Time/dt);
% Nombre de colonnes
Col     = n;
Res = zeros(Ligne+1,Col+1);
Res(1,1:Col+1) = [TempsT T];

% indices des coleurs pour affichage en couleur : pas necessaire
cci =0;    ccj =0;   cck =0;


% formation de la matrice constante
Matrice = zeros(n,n); 
for LL = 2:1:n-1;
    for CC = 1:1:n;
        if LL == CC;
            Matrice(LL,CC) =   CST2;
        end
        if LL == CC +1;
            Matrice(LL,CC) =  -CST1;
        end
        if CC == LL +1
            Matrice(LL,CC) =  -CST1;
        end
    end;
end
Matrice(1,1)   =  CST3;
Matrice(1,2)   = -CST4;
Matrice(n,n-1) =  CST4;
Matrice(n,n)   = -CST7;
T    = T';

% Calcul de la matrice inverse 
invM = inv(Matrice);



% La boucle de calcul 
ii = 1;

while TempsT < Time;
    T(1,1)         = CST5 + CST6*T(1,1);
    T(n,1)         = -CST6*T(n,1);
    Tp = invM*T;
    % Affichage graphique des resultats
    plot(X,Tp,'color',[cci ccj cck]);
    hold on;
    % changement de coleur. pas necessaire
    if cci < 0.9
        cci = cci + 0.01;
    else
        if ccj  < 0.9
            ccj = ccj + 0.01;
        else
            cck = cck + 0.01;
            cci =0;
            ccj =0;
        end
    end;

    % Mise a jour du temps et de la temperature
    TempsT = TempsT + dt;
    T = Tp;
    Res(ii+1,1:Col+1)=[TempsT T'];
    ii = ii + 1;
end;

% Affichage des resultats sous forme de tableau: pas necessaire
%printmat(Res,'Resultats');

% Parametres addimentionnels necessaire pour la comparaison avec 
% la methode analytique ;
Biot     = h*L/K
Xbar    =  X'/L;
Fo      = (alpha/L^2)*Res(1:ii,1);

% Utilisation de la solution analytique;
Precision = 10;
[Texact] = fdX32B10T0(Xbar,Fo,Biot,Precision);

% Conversion 
% Retour aux tempertaures en oC
for jj=1:1:n;
    Texact(1:ii,jj)  = Tinfini*Texact(1:ii,jj);
end
% Retour aux temps en s
Tempstime= (L^2/alpha)*Fo;

% Comparaison des deux methodes: Analytique et numerique
figure
xlabel('Temps (s)');
ylabel('Temperature (oC)');
title('Comparaison des deux methodes: Analytique & Numerique ');
hold on ;
plot(Tempstime,Texact(1:ii,1),'r.',Tempstime,Res(1:ii,2),'-b');
plot(Tempstime,Texact(1:ii,2),'r.',Tempstime,Res(1:ii,3),'-b');
plot(Tempstime,Texact(1:ii,n/8),'r.',Tempstime,Res(1:ii,n/8+1),'-b');
plot(Tempstime,Texact(1:ii,n/4),'r.',Tempstime,Res(1:ii,n/4+1),'-b');
plot(Tempstime,Texact(1:ii,n/2),'r.',Tempstime,Res(1:ii,n/2+1),'-b');
plot(Tempstime,Texact(1:ii,n),'r.',Tempstime,Res(1:ii,n+1),'-b');
legend('Solution Analytique','Solution Numerique','Location','Best')
