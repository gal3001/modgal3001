# Projet: Cuisson d'une saucisse

## Table des matieres

- [Modelisation Chaleur en PDF](#modelisation-chaleur-en-pdf)
- [Description du projet](#description-du-projet)
- [Modelisation sur MATLAB](#modelisation-sur-matlab)
  * [Fichiers pour geometrie plane MATLAB](
                                         #fichiers-pour-geometrie-plane-MATLAB)
  * [Fichiers pour geometrie cylindrique MATLAB](
                                   #fichiers-pour-geometrie-cylindrique-MATLAB)
  * [Fichiers pour geometrie sphérique MATLAB](
                                     #fichiers-pour-geometrie-spherique-MATLAB)
- [Code source du rapport](#code-source-du-rapport)

## Modelisation Chaleur en PDF

Il est possible de consulter le rapport en cliquant sur le lien suivant :
[ModPlanCylSphere.pdf](./RapportPDF/ModPlanCylSphere.pdf)

Pour le télécharger appuyer sur le nuage.

## Description du projet

Le projet a pour but d'apprendre l'tilisation de la méthode des éléments 
finis pour prédire la température au sein d’une figure géométrique
lors d'une cuisson.

Cet approxmation est fait à l'aide de la méthode de Gauss.

## Modelisation sur MATLAB

### Fichiers pour geometrie plane MATLAB

- [Model\_Explicite.m](./PlaneMATLAB/Model_Explicite.m)
- [Model\_Implicite.m](./PlaneMATLAB/Model_Implicite.m)
- [Ufunc.m](./PlaneMATLAB/Ufunc.m)
- [fdX32B10T0.m](./PlaneMATLAB/fdX32B10T0.m)
- [feigXIJ.m](./PlaneMATLAB/feigXIJ.m)


### Fichiers pour geometrie cylindrique MATLAB

- [Model\_Explicite\_Implicite.m](./CylindreMATLAB/Model_Explicite_Implicite.m)

### Fichiers pour geometrie spherique MATLAB

## Code source du rapport

Le code source du rapport en `pdf` se trouve dans ModTexFiles.
